/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.88.200
 Source Server Type    : MySQL
 Source Server Version : 50716
 Source Host           : 192.168.88.200:3307
 Source Schema         : task5

 Target Server Type    : MySQL
 Target Server Version : 50716
 File Encoding         : 65001

 Date: 24/11/2023 19:01:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for log_task
-- ----------------------------
DROP TABLE IF EXISTS `log_task`;
CREATE TABLE `log_task`  (
  `PARENT` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIED` datetime(0) NULL DEFAULT NULL,
  `FORMNAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITUSER` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITDATE` datetime(0) NULL DEFAULT NULL,
  `AUTHOR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATED` datetime(0) NULL DEFAULT NULL,
  `FORMID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SUBFORMIDS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `INITIATOR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ISTMP` bit(1) NULL DEFAULT NULL,
  `VERSIONS` int(11) NULL DEFAULT NULL,
  `APPLICATIONID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATEINT` int(11) NULL DEFAULT NULL,
  `STATELABEL` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORNAMES` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `LASTFLOWOPERATION` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIER` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `COAUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `STATELABELINFO` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITNODE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITUSER` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `OPTIONITEM` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SIGN` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `KINGGRIDSIGNATURE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SECRET` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOC_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_ACTUAL_FINISH_TIME` datetime(0) NULL DEFAULT NULL,
  `ITEM_subject` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_founder` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ITEM_status` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_category` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_deadline` datetime(0) NULL DEFAULT NULL,
  `ITEM_person_in_charge` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ITEM_urgency` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_collaborator` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ITEM_belong_project` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_project_No` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_remark` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ITEM_images` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ITEM_attachments` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ITEM_taskNo` decimal(22, 10) NULL DEFAULT NULL,
  `ITEM_ApplyDate` datetime(0) NULL DEFAULT NULL,
  `ITEM_check` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ITEM_feedback` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_feedback_detail` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of log_task
-- ----------------------------

-- ----------------------------
-- Table structure for t_actorhis
-- ----------------------------
DROP TABLE IF EXISTS `t_actorhis`;
CREATE TABLE `t_actorhis`  (
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ACTORID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AGENTID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AGENTNAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TYPE` int(11) NULL DEFAULT NULL,
  `PROCESSTIME` datetime(0) NULL DEFAULT NULL,
  `ATTITUDE` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NODEHIS_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FLOWSTATERT_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOC_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SIGNATURE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `RECEIVERINFO` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ACTIONTIME` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;



DROP TABLE IF EXISTS `tlk_overduetaskstatistics`;
CREATE TABLE `tlk_overduetaskstatistics`  (
  `PARENT` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIED` datetime(0) NULL DEFAULT NULL,
  `FORMNAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITUSER` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITDATE` datetime(0) NULL DEFAULT NULL,
  `AUTHOR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATED` datetime(0) NULL DEFAULT NULL,
  `FORMID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SUBFORMIDS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `INITIATOR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ISTMP` bit(1) NULL DEFAULT NULL,
  `VERSIONS` int(11) NULL DEFAULT NULL,
  `APPLICATIONID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATEINT` int(11) NULL DEFAULT NULL,
  `STATELABEL` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORNAMES` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `LASTFLOWOPERATION` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIER` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `COAUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `STATELABELINFO` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITNODE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITUSER` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `OPTIONITEM` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SIGN` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `KINGGRIDSIGNATURE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SECRET` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_PERSON_IN_CHARGE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ITEM_OVERDUENUMBER` decimal(22, 10) NULL DEFAULT NULL,
  `ITEM_PROJECT_NO` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_TASK_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ITEM_OVERDUETIME` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Records of t_actorhis
-- ----------------------------

-- ----------------------------
-- Table structure for t_actorrt
-- ----------------------------
DROP TABLE IF EXISTS `t_actorrt`;
CREATE TABLE `t_actorrt`  (
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ACTORID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ISPROCESSED` bit(1) NULL DEFAULT NULL,
  `TYPE` int(11) NULL DEFAULT NULL,
  `NODERT_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FLOWSTATERT_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOC_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DEADLINE` datetime(0) NULL DEFAULT NULL,
  `PENDING` bit(1) NULL DEFAULT NULL,
  `ISREAD` int(11) NULL DEFAULT NULL,
  `DOMAINID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `APPROVAL_POSITION` int(11) NULL DEFAULT NULL,
  `REMINDER_TIMES` int(11) NULL DEFAULT NULL,
  `BAK` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_actorrt
-- ----------------------------

-- ----------------------------
-- Table structure for t_checkin
-- ----------------------------
DROP TABLE IF EXISTS `t_checkin`;
CREATE TABLE `t_checkin`  (
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `WIDGET_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `APPLICATION_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAIN_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `USER_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `USER_NAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CHECKIN_TIME` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_checkin
-- ----------------------------

-- ----------------------------
-- Table structure for t_circulator
-- ----------------------------
DROP TABLE IF EXISTS `t_circulator`;
CREATE TABLE `t_circulator`  (
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `USERID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOC_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NODERT_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FLOWSTATERT_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CCTIME` datetime(0) NULL DEFAULT NULL,
  `READTIME` datetime(0) NULL DEFAULT NULL,
  `DEADLINE` datetime(0) NULL DEFAULT NULL,
  `ISREAD` int(11) NULL DEFAULT NULL,
  `DOMAINID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SUMMARY` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `VERSION` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_circulator
-- ----------------------------

-- ----------------------------
-- Table structure for t_coactorrt
-- ----------------------------
DROP TABLE IF EXISTS `t_coactorrt`;
CREATE TABLE `t_coactorrt`  (
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `USERID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOC_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NODERT_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FLOWSTATERT_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATE_TIME` datetime(0) NULL DEFAULT NULL,
  `READTIME` datetime(0) NULL DEFAULT NULL,
  `DEADLINE` datetime(0) NULL DEFAULT NULL,
  `ISREAD` int(11) NULL DEFAULT NULL,
  `DOMAINID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VERSION` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_coactorrt
-- ----------------------------

-- ----------------------------
-- Table structure for t_comment
-- ----------------------------
DROP TABLE IF EXISTS `t_comment`;
CREATE TABLE `t_comment`  (
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FLAG` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `APPLICATION_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAIN_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `USER_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `USER_NAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `COMMENTS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `UNLIKE_NUM` int(11) NULL DEFAULT NULL,
  `LIKE_NUM` int(11) NULL DEFAULT NULL,
  `CREATE_DATE` datetime(0) NULL DEFAULT NULL,
  `PARENT_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_comment
-- ----------------------------

-- ----------------------------
-- Table structure for t_counter
-- ----------------------------
DROP TABLE IF EXISTS `t_counter`;
CREATE TABLE `t_counter`  (
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `COUNTER` int(11) NULL DEFAULT NULL,
  `NAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_counter
-- ----------------------------

-- ----------------------------
-- Table structure for t_document
-- ----------------------------
DROP TABLE IF EXISTS `t_document`;
CREATE TABLE `t_document`  (
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LASTMODIFIED` datetime(0) NULL DEFAULT NULL,
  `FORMNAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITDATE` datetime(0) NULL DEFAULT NULL,
  `AUTHOR` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHORDEPTID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATED` datetime(0) NULL DEFAULT NULL,
  `FORMID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SUBFORMIDS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ISTMP` bit(1) NULL DEFAULT NULL,
  `VERSIONS` int(11) NULL DEFAULT NULL,
  `SORTID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATELABEL` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `INITIATOR` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITUSER` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORNAMES` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `LASTFLOWOPERATION` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PARENT` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATE` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATEINT` int(11) NULL DEFAULT NULL,
  `LASTMODIFIER` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `COAUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `STATELABELINFO` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITNODE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITUSER` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `OPTIONITEM` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SIGN` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SECRET` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MAPPINGID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_document
-- ----------------------------

-- ----------------------------
-- Table structure for t_flow_proxy
-- ----------------------------
DROP TABLE IF EXISTS `t_flow_proxy`;
CREATE TABLE `t_flow_proxy`  (
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FLOWNAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FLOWID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DESCRIPTION` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATE` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AGENTS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `AGENTSNAME` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `OWNER` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VERSION` int(11) NULL DEFAULT NULL,
  `PROXYMODE` int(11) NULL DEFAULT NULL,
  `STARTPROXYTIME` datetime(0) NULL DEFAULT NULL,
  `ENDPROXYTIME` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_flow_proxy
-- ----------------------------

-- ----------------------------
-- Table structure for t_flowhistory
-- ----------------------------
DROP TABLE IF EXISTS `t_flowhistory`;
CREATE TABLE `t_flowhistory`  (
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FLOWSTATERT_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FLOWID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NODEHIS_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STARTNODENAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ENDNODENAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOC_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ATTITUDE` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PROCESSTIME` datetime(0) NULL DEFAULT NULL,
  `FLOWOPERATION` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STARTNODEID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ENDNODEID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AGENTNAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AGENTID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SIGNATURE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ACTORID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RECEIVERINFO` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ACTIONTIME` datetime(0) NULL DEFAULT NULL,
  `FLOWNAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `INITIATOR` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `INITIATORID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SUMMARY` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `FIRSTPROCESSTIME` datetime(0) NULL DEFAULT NULL,
  `STATELABEL` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `DOMAINID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_flowhistory
-- ----------------------------

-- ----------------------------
-- Table structure for t_flowstatert
-- ----------------------------
DROP TABLE IF EXISTS `t_flowstatert`;
CREATE TABLE `t_flowstatert`  (
  `ID` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DOCID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FLOWID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATE` int(11) NULL DEFAULT NULL,
  `PARENT` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FLOWNAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FLOWXML` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `LASTMODIFIERID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIED` datetime(0) NULL DEFAULT NULL,
  `APPLICATIONID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SUBFLOWNODEID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `COMPLETE` int(11) NULL DEFAULT NULL,
  `CALLBACK` int(11) NULL DEFAULT NULL,
  `TOKEN` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATELABEL` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `INITIATOR` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITUSER` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORNAMES` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `AUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `COAUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `LASTFLOWOPERATION` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITDATE` datetime(0) NULL DEFAULT NULL,
  `SUB_POSITION` int(11) NULL DEFAULT NULL,
  `ISARCHIVED` int(11) NULL DEFAULT NULL,
  `ISTERMINATED` bit(1) NULL DEFAULT NULL,
  `PREV_AUDIT_NODE` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PREV_AUDIT_USER` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `WORKFLOW_TYPE` int(11) NULL DEFAULT NULL,
  `ACTIONTIME` datetime(0) NULL DEFAULT NULL,
  `SUMMARY` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `INITIATORID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FIRSTPROCESSTIME` datetime(0) NULL DEFAULT NULL,
  `DOMAINID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_flowstatert
-- ----------------------------

-- ----------------------------
-- Table structure for t_log
-- ----------------------------
DROP TABLE IF EXISTS `t_log`;
CREATE TABLE `t_log`  (
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `OPERATORID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OPERATOR` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TYPE` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OPERATIONTIME` datetime(0) NULL DEFAULT NULL,
  `SOURCEID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SOURCE` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DESCRIPTION` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `APPLICATIONID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_log
-- ----------------------------

-- ----------------------------
-- Table structure for t_logs
-- ----------------------------
DROP TABLE IF EXISTS `t_logs`;
CREATE TABLE `t_logs`  (
  `ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `OPERATORID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OPERATOR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OPERATIONTIME` datetime(0) NULL DEFAULT NULL,
  `OPERATIONTYPE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SOURCEID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PARENTSOURCEID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SOURCE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SOURCETYPE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DESCRIPTION` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `APPLICATIONID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_logs
-- ----------------------------

-- ----------------------------
-- Table structure for t_nodert
-- ----------------------------
DROP TABLE IF EXISTS `t_nodert`;
CREATE TABLE `t_nodert`  (
  `ID` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NODEID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FLOWID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOCID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FLOWSTATERT_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NOTIFIABLE` bit(1) NULL DEFAULT NULL,
  `DOMAINID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATELABEL` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FLOWOPTION` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SPLITTOKEN` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PASSCONDITION` int(11) NULL DEFAULT NULL,
  `PARENTNODERTID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DEADLINE` datetime(0) NULL DEFAULT NULL,
  `ORDERLY` bit(1) NULL DEFAULT NULL,
  `APPROVAL_POSITION` int(11) NULL DEFAULT NULL,
  `STATE` int(11) NULL DEFAULT NULL,
  `LASTPROCESSTIME` datetime(0) NULL DEFAULT NULL,
  `REMINDER_TIMES` int(11) NULL DEFAULT NULL,
  `ACTIONTIME` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_nodert
-- ----------------------------

-- ----------------------------
-- Table structure for t_relationhis
-- ----------------------------
DROP TABLE IF EXISTS `t_relationhis`;
CREATE TABLE `t_relationhis`  (
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ACTIONTIME` datetime(0) NULL DEFAULT NULL,
  `PROCESSTIME` datetime(0) NULL DEFAULT NULL,
  `STARTNODENAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FLOWID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FLOWNAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOCID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ENDNODEID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ENDNODENAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STARTNODEID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ISPASSED` bit(1) NULL DEFAULT NULL,
  `ATTITUDE` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITOR` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FLOWOPERATION` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `REMINDERCOUNT` int(11) NULL DEFAULT NULL,
  `FLOWSTATERT_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_relationhis
-- ----------------------------

-- ----------------------------
-- Table structure for t_shortmessage_received
-- ----------------------------
DROP TABLE IF EXISTS `t_shortmessage_received`;
CREATE TABLE `t_shortmessage_received`  (
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CONTENT` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SENDER` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RECEIVER` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RECEIVEDATE` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATUS` int(11) NULL DEFAULT NULL,
  `PARENT` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOCID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATED` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_shortmessage_received
-- ----------------------------

-- ----------------------------
-- Table structure for t_shortmessage_submit
-- ----------------------------
DROP TABLE IF EXISTS `t_shortmessage_submit`;
CREATE TABLE `t_shortmessage_submit`  (
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CONTENTTYPE` int(11) NULL DEFAULT NULL,
  `TITLE` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CONTENT` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SENDDATE` datetime(0) NULL DEFAULT NULL,
  `REPLYCODE` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SENDER` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RECEIVER` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SUBMISSION` bit(1) NULL DEFAULT NULL,
  `ISFAILURE` bit(1) NULL DEFAULT NULL,
  `ISREPLY` bit(1) NULL DEFAULT NULL,
  `ISTRASH` bit(1) NULL DEFAULT NULL,
  `ISDRAFT` bit(1) NULL DEFAULT NULL,
  `NEEDREPLY` bit(1) NULL DEFAULT NULL,
  `MASS` bit(1) NULL DEFAULT NULL,
  `DOCID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RECEIVERUSERID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RECEIVERNAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_shortmessage_submit
-- ----------------------------

-- ----------------------------
-- Table structure for t_trigger
-- ----------------------------
DROP TABLE IF EXISTS `t_trigger`;
CREATE TABLE `t_trigger`  (
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TOKEN` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_TYPE` int(11) NULL DEFAULT NULL,
  `JOB_DATA` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `STATE` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DEADLINE` bigint(20) NULL DEFAULT NULL,
  `IS_LOOP` bit(1) NULL DEFAULT NULL,
  `APPLICATIONID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOCUMENTS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `RUNTIMES` int(11) NULL DEFAULT NULL,
  `LASTMODIFIDATE` datetime(0) NULL DEFAULT NULL,
  `TASKID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_trigger
-- ----------------------------

-- ----------------------------
-- Table structure for t_upload
-- ----------------------------
DROP TABLE IF EXISTS `t_upload`;
CREATE TABLE `t_upload`  (
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IMGBINARY` mediumblob NULL,
  `FIELDID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TYPE` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FILESIZE` int(11) NULL DEFAULT NULL,
  `USERID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MODIFYDATE` datetime(0) NULL DEFAULT NULL,
  `PATH` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `FOLDERPATH` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SOURCEFILEID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VERSIONNO` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOCID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RELATEMSG` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_upload
-- ----------------------------

-- ----------------------------
-- Table structure for t_workflow_reminder_history
-- ----------------------------
DROP TABLE IF EXISTS `t_workflow_reminder_history`;
CREATE TABLE `t_workflow_reminder_history`  (
  `ID` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REMINDER_CONTENT` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `USER_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `USER_NAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NODE_NAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOC_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FLOW_INSTANCE_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PROCESS_TIME` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of t_workflow_reminder_history
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_apilog
-- ----------------------------
DROP TABLE IF EXISTS `tlk_apilog`;
CREATE TABLE `tlk_apilog`  (
  `PARENT` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIED` datetime(0) NULL DEFAULT NULL,
  `FORMNAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATE` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITUSER` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITDATE` datetime(0) NULL DEFAULT NULL,
  `AUTHOR` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHORDEPTID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATED` datetime(0) NULL DEFAULT NULL,
  `FORMID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SUBFORMIDS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `INITIATOR` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ISTMP` bit(1) NULL DEFAULT NULL,
  `VERSIONS` int(11) NULL DEFAULT NULL,
  `APPLICATIONID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATEINT` int(11) NULL DEFAULT NULL,
  `STATELABEL` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORNAMES` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `LASTFLOWOPERATION` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIER` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `COAUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `STATELABELINFO` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITNODE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITUSER` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `OPTIONITEM` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SIGN` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `KINGGRIDSIGNATURE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SECRET` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_APINAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_IP` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_REQUESTTIME` datetime(0) NULL DEFAULT NULL,
  `ITEM_REQUESTPARAMETERS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ITEM_RETURNPARAMETERS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tlk_apilog
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_check_maintenance
-- ----------------------------
DROP TABLE IF EXISTS `tlk_check_maintenance`;
CREATE TABLE `tlk_check_maintenance`  (
  `PARENT` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIED` datetime(0) NULL DEFAULT NULL,
  `FORMNAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITUSER` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITDATE` datetime(0) NULL DEFAULT NULL,
  `AUTHOR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATED` datetime(0) NULL DEFAULT NULL,
  `FORMID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SUBFORMIDS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `INITIATOR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ISTMP` bit(1) NULL DEFAULT NULL,
  `VERSIONS` int(11) NULL DEFAULT NULL,
  `APPLICATIONID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATEINT` int(11) NULL DEFAULT NULL,
  `STATELABEL` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORNAMES` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `LASTFLOWOPERATION` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIER` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `COAUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `STATELABELINFO` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITNODE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITUSER` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `OPTIONITEM` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SIGN` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `KINGGRIDSIGNATURE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SECRET` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_TYPE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_STATE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_REMARKS` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ITEM_TRUEVALUE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tlk_check_maintenance
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_check_maintenance_details
-- ----------------------------
DROP TABLE IF EXISTS `tlk_check_maintenance_details`;
CREATE TABLE `tlk_check_maintenance_details`  (
  `PARENT` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIED` datetime(0) NULL DEFAULT NULL,
  `FORMNAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITUSER` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITDATE` datetime(0) NULL DEFAULT NULL,
  `AUTHOR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATED` datetime(0) NULL DEFAULT NULL,
  `FORMID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SUBFORMIDS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `INITIATOR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ISTMP` bit(1) NULL DEFAULT NULL,
  `VERSIONS` int(11) NULL DEFAULT NULL,
  `APPLICATIONID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATEINT` int(11) NULL DEFAULT NULL,
  `STATELABEL` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORNAMES` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `LASTFLOWOPERATION` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIER` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `COAUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `STATELABELINFO` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITNODE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITUSER` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `OPTIONITEM` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SIGN` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `KINGGRIDSIGNATURE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SECRET` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_CHECK_DESCRIPTION` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tlk_check_maintenance_details
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_fallow_task
-- ----------------------------
DROP TABLE IF EXISTS `tlk_fallow_task`;
CREATE TABLE `tlk_fallow_task`  (
  `PARENT` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIED` datetime(0) NULL DEFAULT NULL,
  `FORMNAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATE` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITUSER` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITDATE` datetime(0) NULL DEFAULT NULL,
  `AUTHOR` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHORDEPTID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATED` datetime(0) NULL DEFAULT NULL,
  `FORMID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SUBFORMIDS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `INITIATOR` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ISTMP` bit(1) NULL DEFAULT NULL,
  `VERSIONS` int(11) NULL DEFAULT NULL,
  `APPLICATIONID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATEINT` int(11) NULL DEFAULT NULL,
  `STATELABEL` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORNAMES` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `LASTFLOWOPERATION` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIER` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `COAUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `STATELABELINFO` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITNODE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITUSER` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `OPTIONITEM` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SIGN` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `KINGGRIDSIGNATURE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SECRET` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_TASK_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_FALLOW_USERID` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tlk_fallow_task
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_generalmasterdata
-- ----------------------------
DROP TABLE IF EXISTS `tlk_generalmasterdata`;
CREATE TABLE `tlk_generalmasterdata`  (
  `PARENT` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIED` datetime(0) NULL DEFAULT NULL,
  `FORMNAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATE` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITUSER` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITDATE` datetime(0) NULL DEFAULT NULL,
  `AUTHOR` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHORDEPTID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATED` datetime(0) NULL DEFAULT NULL,
  `FORMID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SUBFORMIDS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `INITIATOR` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ISTMP` bit(1) NULL DEFAULT NULL,
  `VERSIONS` int(11) NULL DEFAULT NULL,
  `APPLICATIONID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATEINT` int(11) NULL DEFAULT NULL,
  `STATELABEL` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORNAMES` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `LASTFLOWOPERATION` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIER` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `COAUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `STATELABELINFO` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITNODE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITUSER` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `OPTIONITEM` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SIGN` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `KINGGRIDSIGNATURE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SECRET` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_NO` decimal(22, 10) NULL DEFAULT NULL,
  `ITEM_STATE` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_REMARKS` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_TYPE` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_SHOWVALUE` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_TRUEVALUE` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tlk_generalmasterdata
-- ----------------------------
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-07-24 16:59:07', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '0897ZO8tU2tgdiXCisW', '1ma7sVPoEbqGEAAYDFC', '1ma7sVPoEbqGEAAYDFC', '$$$test', '2023-07-24 16:59:07', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '0897ZO8tU2tgdiXCisW', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 1.0000000000, '01', '任务-紧急重要', 'TaskUrgent', '不紧急且不重要', '4', '2Tov5cBoVCI9TioBKiR--__z5fq1CVo9vRuD62tMPl');
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-08-07 10:37:18', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '0897ZO8tU2tgdiXCisW', '1ma7sVPoEbqGEAAYDFC', '1ma7sVPoEbqGEAAYDFC', '$$$test', '2023-08-07 10:36:53', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '0897ZO8tU2tgdiXCisW', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 4.0000000000, '01', '项目类型', 'ProjectType', '自立项', '自立项', '30PnC7pDHiSyK7rnyZu--__z5fq1CVo9vRuD62tMPl');
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-08-07 10:35:08', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '0897ZO8tU2tgdiXCisW', '1ma7sVPoEbqGEAAYDFC', '1ma7sVPoEbqGEAAYDFC', '$$$test', '2023-08-07 10:34:37', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '0897ZO8tU2tgdiXCisW', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 1.0000000000, '01', '项目类型', 'ProjectType', '售前', '售前', '5dkfkwf5OSticHoc2id--__z5fq1CVo9vRuD62tMPl');
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-07-24 16:59:07', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '0897ZO8tU2tgdiXCisW', '1ma7sVPoEbqGEAAYDFC', '1ma7sVPoEbqGEAAYDFC', '$$$test', '2023-07-24 16:59:07', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '0897ZO8tU2tgdiXCisW', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 2.0000000000, '01', '任务状态', 'TaskStatus', '逾期完成', '2', 'bB7jIYfSfjl2xky1F8h--__z5fq1CVo9vRuD62tMPl');
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-07-24 16:59:07', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '0897ZO8tU2tgdiXCisW', '1ma7sVPoEbqGEAAYDFC', '1ma7sVPoEbqGEAAYDFC', '$$$test', '2023-07-24 16:59:07', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '0897ZO8tU2tgdiXCisW', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 3.0000000000, '01', '任务-紧急重要', 'TaskUrgent', '重要不紧急', '3', 'CUPrdVjln5UjwYwJWFl--__z5fq1CVo9vRuD62tMPl');
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-07-24 16:59:07', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '0897ZO8tU2tgdiXCisW', '1ma7sVPoEbqGEAAYDFC', '1ma7sVPoEbqGEAAYDFC', '$$$test', '2023-07-24 16:59:07', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '0897ZO8tU2tgdiXCisW', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 5.0000000000, '01', '项目状态', 'ProjectState', '逾期未完成', '05', 'DdLcpsD9e1uT2vrGYl0--__z5fq1CVo9vRuD62tMPl');
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-08-07 10:36:50', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '0897ZO8tU2tgdiXCisW', '1ma7sVPoEbqGEAAYDFC', '1ma7sVPoEbqGEAAYDFC', '$$$test', '2023-08-07 10:36:13', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '0897ZO8tU2tgdiXCisW', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 3.0000000000, '01', '项目类型', 'ProjectType', '售后', '售后', 'dz76zc8hBJDkDjoJSha--__z5fq1CVo9vRuD62tMPl');
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-07-24 16:59:07', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '0897ZO8tU2tgdiXCisW', '1ma7sVPoEbqGEAAYDFC', '1ma7sVPoEbqGEAAYDFC', '$$$test', '2023-07-24 16:59:07', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '0897ZO8tU2tgdiXCisW', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 4.0000000000, '01', '项目状态', 'ProjectState', '完成', '04', 'IJhKaNzXPnWj5YrwiOs--__z5fq1CVo9vRuD62tMPl');
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-08-07 10:36:10', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '0897ZO8tU2tgdiXCisW', '1ma7sVPoEbqGEAAYDFC', '1ma7sVPoEbqGEAAYDFC', '$$$test', '2023-08-07 10:35:10', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '0897ZO8tU2tgdiXCisW', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 2.0000000000, '01', '项目类型', 'ProjectType', '售中', '售中', 'jii01rzCxMcvnf5bkwz--__z5fq1CVo9vRuD62tMPl');
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-07-24 16:59:07', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '0897ZO8tU2tgdiXCisW', '1ma7sVPoEbqGEAAYDFC', '1ma7sVPoEbqGEAAYDFC', '$$$test', '2023-07-24 16:59:07', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '0897ZO8tU2tgdiXCisW', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 4.0000000000, '01', '任务-紧急重要', 'TaskUrgent', '紧急且重要', '1', 'kFMMAmeBUgCzNoHc2ID--__z5fq1CVo9vRuD62tMPl');
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-07-24 16:59:07', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '0897ZO8tU2tgdiXCisW', '1ma7sVPoEbqGEAAYDFC', '1ma7sVPoEbqGEAAYDFC', '$$$test', '2023-07-24 16:59:07', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '0897ZO8tU2tgdiXCisW', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 1.0000000000, '01', '任务状态', 'TaskStatus', '进行中', '1', 'LQu4mjP5tctv7U8Pgv9--__z5fq1CVo9vRuD62tMPl');
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-07-24 16:59:07', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '0897ZO8tU2tgdiXCisW', '1ma7sVPoEbqGEAAYDFC', '1ma7sVPoEbqGEAAYDFC', '$$$test', '2023-07-24 16:59:07', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '0897ZO8tU2tgdiXCisW', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 3.0000000000, '01', '项目状态', 'ProjectState', '未完成', '03', 'nnw9kIw6oFVDTsIPhKB--__z5fq1CVo9vRuD62tMPl');
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-07-24 16:59:07', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '0897ZO8tU2tgdiXCisW', '1ma7sVPoEbqGEAAYDFC', '1ma7sVPoEbqGEAAYDFC', '$$$test', '2023-07-24 16:59:07', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '0897ZO8tU2tgdiXCisW', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 1.0000000000, '01', '项目状态', 'ProjectState', '进行中', '01', 'PoInRVrV3C1udbwrH6I--__z5fq1CVo9vRuD62tMPl');
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-07-24 16:59:07', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '0897ZO8tU2tgdiXCisW', '1ma7sVPoEbqGEAAYDFC', '1ma7sVPoEbqGEAAYDFC', '$$$test', '2023-07-24 16:59:07', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '0897ZO8tU2tgdiXCisW', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 2.0000000000, '01', '项目状态', 'ProjectState', '逾期完成', '02', 'pptnTxo1bHtm6MG7Lqa--__z5fq1CVo9vRuD62tMPl');
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-08-29 14:26:54', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '06uytJxZClkynprNYxF', '1ma7sVPoEbqGEAAYDFC_tf2qMhGEdJgmS4LRWMY', '1ma7sVPoEbqGEAAYDFC_tf2qMhGEdJgmS4LRWMY', '$$$test', '2023-08-29 14:26:33', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '06uytJxZClkynprNYxF', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 6.0000000000, '01', '任务状态', 'TaskStatus', '临时关闭', '6', 'RO30M73inHCui1fGAg4--__z5fq1CVo9vRuD62tMPl');
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-07-24 16:59:07', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '0897ZO8tU2tgdiXCisW', '1ma7sVPoEbqGEAAYDFC', '1ma7sVPoEbqGEAAYDFC', '$$$test', '2023-07-24 16:59:07', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '0897ZO8tU2tgdiXCisW', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 2.0000000000, '01', '任务-紧急重要', 'TaskUrgent', '紧急不重要', '2', 'TyAl11gPVJauYqebbYF--__z5fq1CVo9vRuD62tMPl');
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-07-24 16:59:07', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '0897ZO8tU2tgdiXCisW', '1ma7sVPoEbqGEAAYDFC', '1ma7sVPoEbqGEAAYDFC', '$$$test', '2023-07-24 16:59:07', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '0897ZO8tU2tgdiXCisW', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 3.0000000000, '01', '任务类别', 'TaskCategory', '抢单任务', '3', 'vBf56XdvVdIkwoQPO89--__z5fq1CVo9vRuD62tMPl');
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-07-24 16:59:07', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '0897ZO8tU2tgdiXCisW', '1ma7sVPoEbqGEAAYDFC', '1ma7sVPoEbqGEAAYDFC', '$$$test', '2023-07-24 16:59:07', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '0897ZO8tU2tgdiXCisW', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 3.0000000000, '01', '任务状态', 'TaskStatus', '未完成', '3', 'W9NvAIlYI01JqoyMKdm--__z5fq1CVo9vRuD62tMPl');
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-07-24 16:59:07', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '0897ZO8tU2tgdiXCisW', '1ma7sVPoEbqGEAAYDFC', '1ma7sVPoEbqGEAAYDFC', '$$$test', '2023-07-24 16:59:07', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '0897ZO8tU2tgdiXCisW', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 4.0000000000, '01', '任务状态', 'TaskStatus', '完成', '4', 'xnHZNprhfMPYr131hF0--__z5fq1CVo9vRuD62tMPl');
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-07-24 16:59:07', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '0897ZO8tU2tgdiXCisW', '1ma7sVPoEbqGEAAYDFC', '1ma7sVPoEbqGEAAYDFC', '$$$test', '2023-07-24 16:59:07', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '0897ZO8tU2tgdiXCisW', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 5.0000000000, '01', '任务状态', 'TaskStatus', '逾期未完成', '5', 'yHmaWmjPJnaUk5JJNr9--__z5fq1CVo9vRuD62tMPl');
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-07-24 16:59:07', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '0897ZO8tU2tgdiXCisW', '1ma7sVPoEbqGEAAYDFC', '1ma7sVPoEbqGEAAYDFC', '$$$test', '2023-07-24 16:59:07', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '0897ZO8tU2tgdiXCisW', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 1.0000000000, '01', '任务类别', 'TaskCategory', '简单任务', '1', 'YXJXeDjaELOlLw0WwGy--__z5fq1CVo9vRuD62tMPl');
INSERT INTO `tlk_generalmasterdata` VALUES (NULL, '2023-07-24 16:59:07', '主数据管理/GeneralMasterData', NULL, NULL, NULL, '0897ZO8tU2tgdiXCisW', '1ma7sVPoEbqGEAAYDFC', '1ma7sVPoEbqGEAAYDFC', '$$$test', '2023-07-24 16:59:07', '__z5fq1CVo9vRuD62tMPl', '', NULL, b'0', 1, '__rzuvXrZBA0FLQe8om6r', 0, '', '', NULL, '0897ZO8tU2tgdiXCisW', 'IyYOI14wXLswP0d7Fsz', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', NULL, NULL, NULL, NULL, NULL, NULL, 2.0000000000, '01', '任务类别', 'TaskCategory', '复杂任务', '2', 'zoq6BtUiV8OXw7Xf6Bp--__z5fq1CVo9vRuD62tMPl');

-- ----------------------------
-- Table structure for tlk_project
-- ----------------------------
DROP TABLE IF EXISTS `tlk_project`;
CREATE TABLE `tlk_project`  (
  `PARENT` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIED` datetime(0) NULL DEFAULT NULL,
  `FORMNAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATE` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITUSER` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITDATE` datetime(0) NULL DEFAULT NULL,
  `AUTHOR` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHORDEPTID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATED` datetime(0) NULL DEFAULT NULL,
  `FORMID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SUBFORMIDS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `INITIATOR` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ISTMP` bit(1) NULL DEFAULT NULL,
  `VERSIONS` int(11) NULL DEFAULT NULL,
  `APPLICATIONID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATEINT` int(11) NULL DEFAULT NULL,
  `STATELABEL` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORNAMES` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `LASTFLOWOPERATION` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIER` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `COAUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `STATELABELINFO` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITNODE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITUSER` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `OPTIONITEM` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SIGN` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `KINGGRIDSIGNATURE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SECRET` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_PROJECTNO` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_STATUS` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_SUBJECT` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_PERSON_IN_CHARGE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ITEM_COLLABORATOR` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ITEM_STARTDATE` datetime(0) NULL DEFAULT NULL,
  `ITEM_ESTIMATE_FINISH_DATE` datetime(0) NULL DEFAULT NULL,
  `ITEM_REMARK` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ITEM_FOUNDER` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ITEM_APPLYDATE` datetime(0) NULL DEFAULT NULL,
  `ITEM_TYPE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tlk_project
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_task
-- ----------------------------
DROP TABLE IF EXISTS `tlk_task`;
CREATE TABLE `tlk_task`  (
  `PARENT` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIED` datetime(0) NULL DEFAULT NULL,
  `FORMNAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITUSER` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITDATE` datetime(0) NULL DEFAULT NULL,
  `AUTHOR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATED` datetime(0) NULL DEFAULT NULL,
  `FORMID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SUBFORMIDS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `INITIATOR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ISTMP` bit(1) NULL DEFAULT NULL,
  `VERSIONS` int(11) NULL DEFAULT NULL,
  `APPLICATIONID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATEINT` int(11) NULL DEFAULT NULL,
  `STATELABEL` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORNAMES` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `LASTFLOWOPERATION` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIER` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `COAUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `STATELABELINFO` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITNODE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITUSER` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `OPTIONITEM` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SIGN` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `KINGGRIDSIGNATURE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SECRET` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_ACTUAL_FINISH_TIME` datetime(0) NULL DEFAULT NULL,
  `ITEM_subject` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_founder` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ITEM_status` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_category` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_deadline` datetime(0) NULL DEFAULT NULL,
  `ITEM_person_in_charge` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ITEM_urgency` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_collaborator` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ITEM_belong_project` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_project_No` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_remark` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ITEM_images` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ITEM_attachments` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ITEM_taskNo` decimal(22, 10) NULL DEFAULT NULL,
  `ITEM_ApplyDate` datetime(0) NULL DEFAULT NULL,
  `ITEM_check` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ITEM_feedback` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_feedback_detail` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `idx_status`(`ITEM_status`) USING BTREE,
  INDEX `idx_STATELABEL`(`STATELABEL`) USING BTREE,
  INDEX `idx_person_in_charge`(`ITEM_person_in_charge`(600)) USING BTREE,
  INDEX `idx_deadline`(`ITEM_deadline`) USING BTREE,
  INDEX `idx_collaborator`(`ITEM_collaborator`(600)) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tlk_task
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_transfer
-- ----------------------------
DROP TABLE IF EXISTS `tlk_transfer`;
CREATE TABLE `tlk_transfer`  (
  `PARENT` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIED` datetime(0) NULL DEFAULT NULL,
  `FORMNAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATE` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITUSER` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITDATE` datetime(0) NULL DEFAULT NULL,
  `AUTHOR` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHORDEPTID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATED` datetime(0) NULL DEFAULT NULL,
  `FORMID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SUBFORMIDS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `INITIATOR` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ISTMP` bit(1) NULL DEFAULT NULL,
  `VERSIONS` int(11) NULL DEFAULT NULL,
  `APPLICATIONID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATEINT` int(11) NULL DEFAULT NULL,
  `STATELABEL` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORNAMES` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `LASTFLOWOPERATION` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIER` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `COAUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `STATELABELINFO` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITNODE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITUSER` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `OPTIONITEM` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SIGN` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `KINGGRIDSIGNATURE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SECRET` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_TRANSFEROR` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ITEM_TASKID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tlk_transfer
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_updatedeadline
-- ----------------------------
DROP TABLE IF EXISTS `tlk_updatedeadline`;
CREATE TABLE `tlk_updatedeadline`  (
  `PARENT` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIED` datetime(0) NULL DEFAULT NULL,
  `FORMNAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITUSER` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITDATE` datetime(0) NULL DEFAULT NULL,
  `AUTHOR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATED` datetime(0) NULL DEFAULT NULL,
  `FORMID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SUBFORMIDS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `INITIATOR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ISTMP` bit(1) NULL DEFAULT NULL,
  `VERSIONS` int(11) NULL DEFAULT NULL,
  `APPLICATIONID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATEINT` int(11) NULL DEFAULT NULL,
  `STATELABEL` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORNAMES` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `LASTFLOWOPERATION` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIER` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `COAUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `STATELABELINFO` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITNODE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITUSER` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `OPTIONITEM` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SIGN` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `KINGGRIDSIGNATURE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SECRET` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_UPDATETIME` datetime(0) NULL DEFAULT NULL,
  `ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ITEM_REMARK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_SELECTID` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tlk_updatedeadline
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_work_hours_record
-- ----------------------------
DROP TABLE IF EXISTS `tlk_work_hours_record`;
CREATE TABLE `tlk_work_hours_record`  (
  `PARENT` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIED` datetime(0) NULL DEFAULT NULL,
  `FORMNAME` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATE` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITUSER` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITDATE` datetime(0) NULL DEFAULT NULL,
  `AUTHOR` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHORDEPTID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATED` datetime(0) NULL DEFAULT NULL,
  `FORMID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SUBFORMIDS` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `INITIATOR` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ISTMP` bit(1) NULL DEFAULT NULL,
  `VERSIONS` int(11) NULL DEFAULT NULL,
  `APPLICATIONID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STATEINT` int(11) NULL DEFAULT NULL,
  `STATELABEL` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORNAMES` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `LASTFLOWOPERATION` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LASTMODIFIER` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `COAUDITORLIST` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `STATELABELINFO` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITNODE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PREVAUDITUSER` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `OPTIONITEM` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SIGN` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `KINGGRIDSIGNATURE` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `SECRET` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_REG_USERID` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ITEM_REG_DATE` datetime(0) NULL DEFAULT NULL,
  `ITEM_WORK_HOURS` decimal(22, 10) NULL DEFAULT NULL,
  `ITEM_REMARK` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ITEM_TASK_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_PROJECT_NO` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ITEM_TASK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;


-- ----------------------------
-- Records of tlk_work_hours_record
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
