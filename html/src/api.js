// 引入 axios
import axios from 'axios';

// cp是全局变量，之前是通过使用process.env.NODE_ENV==='production'来判断打包的是哪一个路径，在tomcat下是..(由于在Tomcat是domain在obpm里面，但是要找到根目录的路径)，在本地是obpm
const obpmFilePath = obpmConfig.obpmFilePath
const contextPath = obpmConfig.contextPath; 
const instance = axios.create({
});
instance.interceptors.request.use(
    config => {
        if(config.method.toLowerCase() == 'put' || config.method.toLowerCase() == 'post'){
            // config.headers['adminToken'] = localStorage.getItem('adminToken');
            // if(config.data && config.data.loginpwd && config.data.loginpwd.length > 60){
            //     Message.error('密码长度不能大于60位')
            //     return false
            // }
            return config
        }else{
            // config.headers['adminToken'] = localStorage.getItem('adminToken');
            return config
        }
    },
    err => {
        return Promise.reject(err)
    }
);

//http response 拦截器
instance.interceptors.response.use(
    response => {
        console.log("axios.interceptors.response");
        return response;
    },
    error => {
        //未登录
        if (error.response.status == 401) {
            window.location = "index.html#/login";
        }
        else {
            return Promise.reject(error)
        }
    }
);

instance.defaults.withCredentials = true;



/**
 * 获取任务
 */
export function getTaskQuery(appName,data,{ onSucess, onError }) {
	instance.post(obpmFilePath + '/magic-api/'+appName+'/task_query',data).then(function (response) {
		if (onSucess) onSucess(response);
	}).catch(
		function (error) {
			if (onError) onError(error);
		}
	);
}

/**
 * 抢单
 */
export function getTaskGradOrder(appName,data,{ onSucess, onError }) {
	instance.post(obpmFilePath + '/magic-api/'+appName+'/task_grad_order',data).then(function (response) {
		if (onSucess) onSucess(response);
	}).catch(
		function (error) {
			if (onError) onError(error);
		}
	);
}
