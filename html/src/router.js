import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)


const router = new Router({
  mode: 'hash',
  routes: [
    // {
    //   path: '/',
    //   redirect: '/login',
    // },
    {
      path: '/task_query',
      name: 'task_query',
      component: () => import('./components/task_query.vue')
    },
    
  ]
});
// router.beforeEach((to, from, next) => {
//   next()
// })
export default router;